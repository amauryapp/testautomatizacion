*** Settings ***
Library     SeleniumLibrary
*** Variables ***
${navegador}    chrome
${url}      http://www.super99.com/recetas
${titulo1}   Recetas de Sal | super99.com
${titulo2}   Recetas de Sal | super99.com
*** KeyWords ***
Abrir Navegador y esperar
    Open Browser    ${url}    ${navegador}
    Wait Until Element Is Visible   xpath=//*[@id="block-super-99-content"]/div/div[1]/a[1]/button
*** Test Cases ***
S99002 - Verificación de las recetas de sal
    Abrir Navegador y esperar
    Click Element   xpath=//*[@id="block-super-99-content"]/div/div[1]/div[1]/div/header/div[2]/a/button
    Title Should Be     ${titulo1}
    Wait Until Element Is Visible    xpath=//*[@id="block-super-99-content"]/div/div/header/h2
    Title Should Be     ${titulo2}
    Close Browser



