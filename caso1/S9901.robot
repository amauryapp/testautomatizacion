*** Settings ***
Library     SeleniumLibrary
*** Variables ***
${navegador}    chrome
${url}      http://www.super99.com
*** KeyWords ***
Abrir Navegador y esperar
    Open Browser    ${url}    ${navegador}
    Wait Until Element Is Visible   xpath=//*[@id="block-views-block-destacado-block-1"]/div/div/section/div/h5/div/img
*** Test Cases ***
S9901 - Verificación de la página de recetas
    Abrir Navegador y esperar
    Click Element       xpath=//*[@id="block-views-block-destacado-block-1"]/div/div/section/div/div/div/div/div[1]/div/a/img
    Title Should Be     Recetas | super99.com
    Close Browser
