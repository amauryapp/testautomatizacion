*** Settings ***
Library     HttpLibrary.HTTP
Library     String
Library     Collections

*** Variables ***
${SCHEME}     http
${HOST}       jsonplaceholder.typicode.com

*** Keywords ***

Setup Context
    Create HTTP Context   ${HOST}    ${SCHEME}
    Set Request Header   Content-Type    application/json;

Success Call
    [Arguments]   ${method}   ${uri}   ${response_code}   ${payload}=
    Log Many   ${method}   ${uri}   ${payload}
    Set Request Body   ${payload}
    Run Keyword And Continue on Failure   ${method}   ${uri}
    Log Response Status
    Log Response Body
    Response Status Code Should Equal   ${response_code}
    ${body}=  Get Response Body
    Return From Keyword   ${body}

Failure Call
    [Arguments]         ${method}       ${uri}          ${response_code}  ${payload}=
    Log Many   ${method}   ${uri}   ${payload}
    Set Request Body   ${payload}
    Run Keyword And Ignore Error   ${method}   ${uri}
    Log Response Status
    Log Response Body
    Response Status Code Should Equal   ${response_code}
    ${body}=  Get Response Body
    Return From Keyword   ${body}

Validate Request Errors
    [Arguments]  ${method}  ${endpoint}   ${message}   ${code}   ${payload}=
    Failure Call   ${method}   ${endpoint}   ${code}   ${payload}
    ${body}=  Get Response Body
    ${erorrMsg}=  Get Json Value   ${body}  /message
    Should Be Equal   ${erorrMsg}   ${message}
